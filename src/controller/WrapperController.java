package controller;

import component.Agency;
import component.Agent;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import javax.annotation.PostConstruct;
import java.io.IOException;

public class WrapperController extends MovableController {
    @FXML
    private Circle avatarShape_CRL;
    @FXML
	private GridPane root_GRD;
    @FXML
	private Button agencies_BTN;
    @FXML
	private Button agents_BTN;
    @FXML
	private Button settings_BTN;
    @FXML
	private Button quit_BTN;
    private Button[] menuItemButtons;

    public WrapperController(Stage primaryStage) {
        super(primaryStage);
    }

    @PostConstruct
	public void initialize() {
    	loadProfilePicture();
    	menuItemButtons = new Button[4];
    	menuItemButtons[0] = agencies_BTN;
    	menuItemButtons[1] = agents_BTN;
    	menuItemButtons[2] = settings_BTN;
    	menuItemButtons[3] = quit_BTN;
	}

	/**
	 * Loads profile picture from server.
	 */
	public void loadProfilePicture() {
        Image avatar = new Image("http://localhost/profile.jpg", false);
        avatarShape_CRL.setFill(new ImagePattern(avatar));
    }

	/**
	 * Loads agencies list and instantiate Agency class which handles their rendering into JavaFX nodes.
	 */
	public void loadAgencies() {
		try {
			deselectAllMenuItem();
			agencies_BTN.getStyleClass().add("selected");

			FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/Agencies.fxml"));
			AgenciesController controller = new AgenciesController(this);
			loader.setController(controller);
			Parent container = loader.load();

			// We should send a request to the API but we're actually mocking

			controller.getContainer().getChildren().addAll(
					new Agency(this, 1, "Agence de Oued El Bacha", 5, 1203).displayCollapsed(),
					new Agency(this, 1, "Agence de Niamakoro", 3, 123).displayCollapsed(),
					new Agency(this, 1, "Agence de Lamiaa", 27, 4111).displayCollapsed(),
					new Agency(this, 1, "Agence de Bozola", 13, 2123).displayCollapsed()
			);

			root_GRD.add(container, 1, 1);
		}
		catch (IOException ex) {
			System.out.println("Could not load Agencies.fxml");
			ex.printStackTrace();
		}
	}

	/**
	 * Loads an agency from fxml file. Rendering is not handled by Agency class because the node on its own is a whole
	 * scene that is wrapped alone by the wrapper. However its constructor takes an Agency object which encapsulate
	 * data gathered from server.
	 * @param agencyId - Used to identify a single agency in request to server.
	 */
	public void loadAgency(int agencyId) {
		try {
			deselectAllMenuItem();
			agencies_BTN.getStyleClass().add("selected");

			FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/Agency.fxml"));
			AgencyController controller = new AgencyController(new Agency(this));
			loader.setController(controller);
			Parent container = loader.load();

			root_GRD.add(container, 1, 1);
		}
		catch (IOException ex) {
			System.out.println("Could not load Agency.fxml");
			ex.printStackTrace();
		}
	}

	public void loadAgents() {
		try {
			deselectAllMenuItem();
			agents_BTN.getStyleClass().add("selected");

			FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/Agents.fxml"));
			AgentsController controller = new AgentsController(this);
			loader.setController(controller);
			Parent container = loader.load();

			controller.getContainer().getChildren().addAll(
					new Agent(this).displayCollapsed(),
					new Agent(this).displayCollapsed(),
					new Agent(this).displayCollapsed(),
					new Agent(this).displayCollapsed(),
					new Agent(this).displayCollapsed()
			);

			root_GRD.add(container, 1, 1);
		}
		catch (IOException ex) {
			System.out.println("Could not load Agents.fxml");
			ex.printStackTrace();
		}


	}

	public void loadAgent(int agentId) {
		try {
			deselectAllMenuItem();
			agents_BTN.getStyleClass().add("selected");

			FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/Agent.fxml"));
			AgentController controller = new AgentController(new Agent(this));
			loader.setController(controller);
			Parent container = loader.load();

			root_GRD.add(container, 1, 1);
		}
		catch (IOException ex) {
			System.out.println("Could not load Agent.fxml");
			ex.printStackTrace();
		}
	}

	/**
	 * Load a form which allows user to create an agency
	 */
	public void loadCreateAgency() {
		try {
			agencies_BTN.getStyleClass().add("selected");

			FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/CreateAgency.fxml"));
			CreateAgencyController controller = new CreateAgencyController(this);
			loader.setController(controller);
			Parent container = loader.load();

			root_GRD.add(container, 1, 1);
		}
		catch (IOException ex) {
			System.out.println("Could not load CreateAgency.fxml");
			ex.printStackTrace();
		}
	}

	public void loadCreateAgent() {
		try {
			agencies_BTN.getStyleClass().add("selected");

			FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/CreateAgent.fxml"));
			CreateAgentController controller = new CreateAgentController(this);
			loader.setController(controller);
			Parent container = loader.load();

			root_GRD.add(container, 1, 1);
		}
		catch (IOException ex) {
			System.out.println("Could not load CreateAgent.fxml");
			ex.printStackTrace();
		}
	}

	private void deselectAllMenuItem() {
		for (Button button : menuItemButtons) {
			button.getStyleClass().remove("selected");
		}
	}

	/**
	 * Exits the app.
	 */
	public void quit() {
		// TODO: prompt confirm dialog box before actually exiting the app.
		Platform.exit();
	}

}
