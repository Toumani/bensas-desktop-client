package controller;

import exception.ConnectionException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import loader.Loader;
import loader.WrapperLoader;

import java.io.IOException;
import java.sql.SQLException;


public class LoginController extends MovableController {
	@FXML
	private TextField login_TXTF;
	@FXML
	private PasswordField password_PSW;
	@FXML
	private Label error_LBL;

	public LoginController(Stage primaryStage) {
		super(primaryStage);
	}

	public void login() {
		String login = login_TXTF.getText(), password = password_PSW.getText();
		try {
			String token = requestSession(login, password);
			Loader.setToken(token);
			new WrapperLoader().load();
		}
		catch (ConnectionException ex) {
			error_LBL.setText("Échec de l'authentification.\n" + ex.getMessage());
		}
		catch (SQLException ex) {
			error_LBL.setText(ex.getMessage());
		}
		catch (IOException ex) {
			System.out.println("Couldn't load wrapper");
			ex.printStackTrace();
		}
	}

	public String requestSession(String login, String password) throws ConnectionException, SQLException {
		// Mocking
		if (!login.equals("toumani"))
			throw new ConnectionException("Vérifiez le login et le mot de passe.");
		return "d352eazedfs8854a63f6846564c87518f1b1849816a165d";
	}

	public void exit() {
		Platform.exit();
	}
}
