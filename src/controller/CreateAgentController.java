package controller;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import javax.annotation.PostConstruct;

public class CreateAgentController extends WrappedController {
	@FXML
	private TextField firstName_TXTF, lastName_TXTF, login_TXTF;
	@FXML
	private ComboBox agencies_CMB;

	public CreateAgentController(WrapperController wrapperController) {
		super(wrapperController);
	}

	@PostConstruct
	public void initialize() {
		// Populating agencies list combo box
		agencies_CMB.getItems().addAll(
				"Agence de Oued El Bacha",
				"Agence de Bozola",
				"Agence de Markala",
				"Agence de Miftah El Khier"
		);
	}

	public void createAgent() {
		System.out.println("Creating agent " + firstName_TXTF.getText() + " " + lastName_TXTF.getText() + "  with login " + login_TXTF.getText() + " on agency " + agencies_CMB.getSelectionModel().getSelectedItem().toString());
		wrapperController.loadAgents();
	}

	public void loadAgents() {
		wrapperController.loadAgents();
	}
}
