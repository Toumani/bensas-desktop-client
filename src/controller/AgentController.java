package controller;

import component.Agent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

import javax.annotation.PostConstruct;
import java.util.Random;

public class AgentController extends WrappedController {
	@FXML
	private Circle avatarShape_CRL;
	@FXML
	private ComboBox agencies_CMB;

	private Agent agent;

	public AgentController(Agent agent) {
		super(agent.getWrapperController());
		this.agent = agent;
	}

	@PostConstruct
	public void initialize() {
		// Loading agent profile picture
		// TODO: catch IllegalArgumentException for null image
		Image avatar = new Image("http://localhost/agent-" + (new Random().nextInt(4) + 1) + ".jpg", false);
		avatarShape_CRL.setFill(new ImagePattern(avatar));

		// Populating combo box.
		agencies_CMB.getItems().addAll(
				"Agence de Oued El Bacha",
				"Agence de Bozola",
				"Agence de Markala",
				"Agence de Miftah El Khier"
		);

	}

	public void updateAgent() {
		System.out.println("Updating agent with agency : " + agencies_CMB.getSelectionModel().getSelectedItem().toString());
	}

	public void loadAgents() {
		wrapperController.loadAgents();
	}
}
