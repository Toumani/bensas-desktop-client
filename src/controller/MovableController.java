package controller;

import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * Mother class of controllers that need their scene to be move by drag and drop from any of their point.
 * Its mouserPressed() and mouseDragged() method should be associated with respectively onMousePressed and
 * onMouseDragged event of the root container of the scene.
 */
public class MovableController {
	private Stage primaryStage;
	private double xOffset, yOffset;

	/**
	 * Constructor
	 * @param primaryStage - The stage the scene belongs to.
	 */
	public MovableController(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	// These methods could be protected.
	public void mousePressed(MouseEvent event) {
		xOffset = event.getSceneX();
		yOffset = event.getSceneY();
	}

	public void mouseDragged(MouseEvent event) {
		primaryStage.setX(event.getScreenX() - xOffset);
		primaryStage.setY(event.getScreenY() - yOffset);
	}
}
