package controller;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

import javax.annotation.PostConstruct;

public class AgentsController extends WrappedController {
	@FXML
	private Circle plus_CRL;

	public AgentsController(WrapperController wrapperController) {
		super(wrapperController);
	}

	@PostConstruct
	public void initialize() {
		Image plus = new Image("http://localhost/plus.png", false);
		plus_CRL.setFill(new ImagePattern(plus));
	}

	public void loadAgent() {
		System.out.println("Loading agent");
	}

	public void loadCreateAgent() {
		wrapperController.loadCreateAgent();
	}
}
