package controller;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

import javax.annotation.PostConstruct;

public class AgenciesController extends WrappedController {
	@FXML
	private Circle plus_CRL;

	public AgenciesController(WrapperController wrapperController) {
		super(wrapperController);
	}

	@PostConstruct
	public void initialize() {
		loadPlusPicture();
	}

	public void loadPlusPicture() {
		Image plus = new Image("http://localhost/plus.png", false);
		plus_CRL.setFill(new ImagePattern(plus));
	}

	public void loadCreateAgency() {
		wrapperController.loadCreateAgency();
	}

}
