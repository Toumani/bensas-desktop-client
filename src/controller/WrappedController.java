package controller;

import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

public class WrappedController {
	@FXML
	private VBox container_VBX;

	protected WrapperController wrapperController;

	public WrappedController(WrapperController wrapperController) {
		this.wrapperController = wrapperController;
	}

	public VBox getContainer() { return this.container_VBX; }
}
