package controller;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import javax.annotation.PostConstruct;

public class CreateAgencyController extends WrappedController {
	@FXML
	private TextField agentName_TXTF;
	@FXML
	private ListView<String> agents_LST;
	@FXML
	private Button delete_BTN;

	private ObservableList<String> agents;

	public CreateAgencyController(WrapperController wrapperController) {
		super(wrapperController);
		agents = FXCollections.observableArrayList();
	}

	@PostConstruct
	public void initialize() {
		agents_LST.setItems(agents);
		agents_LST.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> observableValue, String oldValue, String newValue) -> {
			if (newValue != null)
				delete_BTN.setDisable(false);
			else
				delete_BTN.setDisable(true);
		});
	}

	public void addAgent() {
		System.out.println("Adding agent: " + agentName_TXTF.getText());
		agents.add(agentName_TXTF.getText());
		agentName_TXTF.setText("");
	}

	public void loadAgencies() {
		wrapperController.loadAgencies();
	}

	public void deleteAgent() {
		String toDelete = agents_LST.getSelectionModel().getSelectedItem();
		agents.remove(toDelete);
	}
}
