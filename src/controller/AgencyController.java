package controller;

import component.Agency;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import javax.annotation.PostConstruct;

import java.util.ArrayList;

public class AgencyController extends WrappedController {
	@FXML
	private HBox summary_HBX;
	@FXML
	private ListView agents_LST;
	@FXML
	private StackPane header_STK;

	private Text agencyName_TXT;
	private TextField agencyName_TXTF;

	private ObservableList<String> agents;
	private WrapperController wrapperController;

	public AgencyController(Agency agency) {
		super(agency.getWrapperController());
		this.agents = FXCollections.observableArrayList();
		this.agents.addAll(
				"Toumani Sidibe - nitrate",
					"Djeneba Dirra - ddiarra",
					"Yagassa Yattara - yyattara",
					"Alpha Attaher - aattaher",
					"Bob Bako - bbako",
					"Corazón Coulibaly - ccoulibaly"
		);
		this.agencyName_TXT = new Text("Agence de Markala");
		this.agencyName_TXT.setFill(Color.WHITE);
		this.agencyName_TXT.setTextAlignment(TextAlignment.CENTER);
		this.agencyName_TXT.setTranslateY(20.);
		this.agencyName_TXT.setWrappingWidth(712.);
		this.agencyName_TXT.setFont(new Font("Cantarell Bold", 46.));
		this.agencyName_TXT.setCursor(Cursor.HAND);

		this.agencyName_TXTF = new TextField(this.agencyName_TXT.getText());
		this.agencyName_TXTF.setPrefSize(712., 50);
		this.agencyName_TXTF.setTranslateY(20.);

		agencyName_TXT.setOnMouseClicked((MouseEvent mouseEvent) -> {
			header_STK.getChildren().remove(agencyName_TXT);
			header_STK.getChildren().addAll(agencyName_TXTF);
		});

		this.agencyName_TXTF.setOnAction((ActionEvent actionEvent) -> {
			agencyName_TXT.setText(agencyName_TXTF.getText());
			header_STK.getChildren().remove(agencyName_TXTF);
			header_STK.getChildren().addAll(agencyName_TXT);
		});
	}

	@PostConstruct
	public void initialize() {
		header_STK.getChildren().addAll(this.agencyName_TXT);

		// Populating chart
		ArrayList<String> months = new ArrayList<>();
		months.add("Août");
		months.add("Septembre");
		months.add("Octobre");
		months.add("Novembre");
		months.add("Décembre");
		CategoryAxis xAxis = new CategoryAxis(FXCollections.observableList(months));
		NumberAxis yAxis = new NumberAxis(1000, 2000, 200);

		XYChart.Series datas = new XYChart.Series();
		datas.setName("Historique des abonnements");
		datas.getData().add(new XYChart.Data("Août", 1203));
		datas.getData().add(new XYChart.Data("Septembre", 1210));
		datas.getData().add(new XYChart.Data("Octobre", 1301));
		datas.getData().add(new XYChart.Data("Novembre", 1415));
		datas.getData().add(new XYChart.Data("Décembre", 1423));

		LineChart<CategoryAxis, NumberAxis> history_CHT = new LineChart(xAxis, yAxis);
		history_CHT.getData().add(datas);
		summary_HBX.getChildren().add(history_CHT);

		// Populating agent list
		agents_LST.setItems(agents);
	}

}
