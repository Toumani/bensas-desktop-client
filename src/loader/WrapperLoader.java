package loader;

import controller.WrapperController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;

public class WrapperLoader extends Loader {
	@Override
	public void load() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/Wrapper.fxml"));
		WrapperController controller = new WrapperController(primaryStage);
		loader.setController(controller);
		Parent root = loader.load();

		// controller.loadProfilePicture();

		Scene scene = new Scene(root, Loader.WRAPPER_WIDTH, Loader.WRAPPER_HEIGHT);

		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
