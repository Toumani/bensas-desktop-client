package loader;

import controller.LoginController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import jfxtras.styles.jmetro8.JMetro;

import java.io.IOException;

/**
 * Loads the login form.
 */
public class LoginLoader extends Loader {
	@Override
	public void load() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/Login.fxml"));
		loader.setController(new LoginController(primaryStage));
		Parent root = loader.load();

		Scene scene = new Scene(root, 600, 400);

		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
