package loader;

import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * Mother class of loader. Keeps session information as user goes throughout the app.
 */
public abstract class Loader {
	protected static Stage primaryStage;
	protected static String token;

	protected static final double WRAPPER_WIDTH = 900.;
	protected static final double WRAPPER_HEIGHT = 570.;

	/**
	 * Loads view from *.fxml file
	 * @throws IOException
	 */
	public abstract void load() throws IOException;

	/**
	 * Keeps primary stage reference so it can be used by all children classes.
	 * @param primaryStage - The stage provided by Main class.
	 */
	public static void setStage(Stage primaryStage) {
		Loader.primaryStage = primaryStage;
		primaryStage.initStyle(StageStyle.UNDECORATED);
	}

	public static String getToken() { return Loader.token; }
	public static void setToken(String token) { Loader.token = token; }
}
