package loader;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Entry class of the application. Has the JavaFX generic layout and calls loader which handles showing up the login
 * form
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Loader.setStage(primaryStage);
        new LoginLoader().load();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
