package component;

import controller.WrapperController;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public abstract class Component {
	protected String name;
	protected WrapperController wrapperController;
	protected int id;

	static final double ITEM_HEIGHT = 150.;
	static final double SUMMARY_OFFSET_X = 159.;
	static final double SUMMARY_OFFSET_Y = 10.;
	static final String AGENCY_ICON = "http://localhost/bank.png";
	static final String AGENT_ICON = "http://localhost/seller.png";

	static final String LIGHT_VIOLET = "#310050";
	static final String LIGHT_GREY = "#CCCCCC";
	static final String DARK_GREY = "#979797";

	public abstract Node displayCollapsed();

	public Component(WrapperController wrapperController) {
		this.wrapperController = wrapperController;
	}

	public WrapperController getWrapperController() { return wrapperController; }

	HBox HBoxBuilder(String icon, String text) {
		HBox hBox = new HBox(10.);
		hBox.setAlignment(Pos.CENTER_LEFT);
		hBox.setPadding(new Insets(0., 0., 0., 15.));
		FontAwesomeIconView icon_FAW = new FontAwesomeIconView();
		icon_FAW.setFill(Color.web(DARK_GREY));
		icon_FAW.setGlyphName(icon);
		icon_FAW.setSize("24.0");
		Text text_TXT = new Text(text);
		text_TXT.setFill(Color.web(DARK_GREY));
		hBox.getChildren().addAll(icon_FAW, text_TXT);
		return hBox;
	}
}
