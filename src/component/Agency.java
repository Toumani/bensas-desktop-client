package component;

import controller.WrapperController;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Agency extends Component {
	private int nbAgents, nbCustommers;

	public String getName() { return name; }

	public void setName(String name) { this.name = name; }

	public int getNbAgents() { return nbAgents; }

	public void setNbAgents(int nbAgents) { this.nbAgents = nbAgents; }

	public Agency(WrapperController wrapperController) {
		super(wrapperController);
	}

	public Agency(WrapperController wrapperController, int id, String name, int nbAgents, int nbCustommers) {
		super(wrapperController);
		this.name = name;
		this.nbAgents = nbAgents;
		this.nbCustommers = nbCustommers;
	}

	@Override
	public Node displayCollapsed() {
		// Root container
		AnchorPane parent = new AnchorPane();
		parent.setPrefHeight(ITEM_HEIGHT);
		parent.setStyle("-fx-background-color: " + LIGHT_VIOLET);

		// Logo
		ImageView logo = new ImageView(new Image(AGENCY_ICON));
		logo.setPreserveRatio(false);
		logo.setFitHeight(ITEM_HEIGHT);
		logo.setFitWidth(ITEM_HEIGHT);
		AnchorPane.setTopAnchor(logo, 0.0);
		AnchorPane.setLeftAnchor(logo, 0.0);
		InnerShadow effect = new InnerShadow(BlurType.GAUSSIAN, Color.BLACK, 28., 0.13, 0.0, 0.0);
		logo.setEffect(effect);

		// Info container
		VBox summaryContainer = new VBox();
		AnchorPane.setTopAnchor(summaryContainer, SUMMARY_OFFSET_Y);
		AnchorPane.setLeftAnchor(summaryContainer, SUMMARY_OFFSET_X);
		summaryContainer.setSpacing(10.);

		// Name text
		Text agencyName = new Text(this.name);
		agencyName.setFont(new Font("Cantarell Regular", 26.0));
		agencyName.setFill(Color.web(LIGHT_GREY));

		// Agents
		HBox agents = HBoxBuilder("USER", this.nbAgents + "");

		// Custommers
		HBox custommers = HBoxBuilder("USERS", this.nbCustommers + "");

		// Manage button
		Button expand = new Button("Gérer");
		expand.setOnAction((ActionEvent actionEvent) -> {
				wrapperController.loadAgency(1);
			}
		);
		AnchorPane.setBottomAnchor(expand, 14.0);
		AnchorPane.setRightAnchor(expand, 14.0);

		summaryContainer.getChildren().addAll(agencyName, agents, custommers);
		parent.getChildren().addAll(logo, summaryContainer, expand);
		return parent;
	}
}
